# BreathingGUI

The python script `BreathingGUI.py` is a Flask application.
The function `get_data` needs to read the data from a file and format it, such that it can be used by `update_data.js`.
Writing to a CSV file still needs to be done.
This will be done using another python script.
Examples of doing this can be seen in [OpenSource_ventilator_lungs](https://github.com/Lasegers/OpenSource_ventilator_lungs).

To run the `BreathingGUI.py` follow the steps on this website: [Flask](https://flask.palletsprojects.com/en/1.0.x/quickstart/).

TODO:
- Add script to read data from Arduino and save it to a CSV file.
- Correctly send data to website.
- Plot data correctly.
