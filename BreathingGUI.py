from threading import Thread
import atexit
from flask import Flask, render_template
import json
import math
import datetime
import serial

app = Flask(__name__)

ser = serial.Serial('/dev/ttyACM0', baudrate=115200)

data = {}
data["Pressure"] = {}
data["Pressure"]["x"] = []
data["Pressure"]["y"] = []
data["TriggerPressure"] = {}
data["TriggerPressure"]["x"] = []
data["TriggerPressure"]["y"] = []
data["bpm"] = {}
data["bpm"]["x"] = []
data["bpm"]["y"] = []
data["Volume"] = {}
data["Volume"]["x"] = []
data["Volume"]["y"] = []

thread = None


def read_arduino_data():
    while True:
        data_line = ser.readline().decode()[:-2]

        if "Pressure" in data_line or "TiggerPressure" in data_line or "bpm" in data_line or "Volume" in data_line:
            values = data_line.split("=")
            data[values[0]]["y"].append(values[1])
            data[values[0]]["x"].append(
                datetime.datetime.now().strftime("%H:%M:%S.%f"))


@app.route("/get_data")
def get_data_request():
    global data

    json_data = json.dumps(data)

    data["Pressure"]["x"].clear()
    data["Pressure"]["y"].clear()

    data["TriggerPressure"]["x"].clear()
    data["TriggerPressure"]["y"].clear()

    data["bpm"]["x"].clear()
    data["bpm"]["y"].clear()

    data["Volume"]["x"].clear()
    data["Volume"]["y"].clear()

    return json_data


@app.route("/settings/save")
def save_settings():
    return "settings saved"


@app.route("/settings")
def settings_screen():
    return render_template("settings_screen.html")


@app.route("/")
def home_screen():
    return render_template("home_screen.html")


def app_start():
    global thread
    thread = Thread(target=read_arduino_data)
    thread.start()


def join_threads():
    global thread
    thread.join()


if __name__ == "__main__":
    atexit.register(join_threads)
    app_start()
    app.run()
