pressure_plot_layout = {
    title: {
        text: 'Pressure',
        font: {
            family: 'Courier New, monospace',
            size: 24
        },
        xref: 'paper',
        x: 0.05
    },
    xaxis: {
        title: {
            text: "Date and time"
        }
    },
    yaxis: {
        title: {
            text: "Value"
        }
    }
};

volume_plot_layout = {
    title: {
        text: 'Volume',
        font: {
            family: 'Courier New, monospace',
            size: 24
        },
        xref: 'paper',
        x: 0.05
    },
    xaxis: {
        title: {
            text: "Date and time"
        }
    },
    yaxis: {
        title: {
            text: "Value"
        }
    }
};

bpm_plot_layout = {
    title: {
        text: 'Breaths Per Minute',
        font: {
            family: 'Courier New, monospace',
            size: 24
        },
        xref: 'paper',
        x: 0.05
    },
    xaxis: {
        title: {
            text: "Date and time"
        }
    },
    yaxis: {
        title: {
            text: "Value"
        }
    }
};

trigger_pressure_plot_layout = {
    title: {
        text: 'Trigger Pressure',
        font: {
            family: 'Courier New, monospace',
            size: 24
        },
        xref: 'paper',
        x: 0.05
    },
    xaxis: {
        title: {
            text: "Date and time"
        }
    },
    yaxis: {
        title: {
            text: "Value"
        }
    }
};
