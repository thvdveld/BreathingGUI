pressure_plot_data = [
    {
        x: [],
        y: []
    }
];



volume_plot_data = [
    {
        x: [],
        y: []
    }
];



bpm_plot_data = [
    {
        x: [],
        y: []
    }
];



trigger_pressure_plot_data = [
    {
        x: [],
        y: []
    }
];


PressurePlot = document.getElementById('pressure_plot');
Plotly.newPlot(PressurePlot, pressure_plot_data, pressure_plot_layout);

VolumePlot = document.getElementById('volume_plot');
Plotly.newPlot(VolumePlot, volume_plot_data, volume_plot_layout);

BPMPlot = document.getElementById('bpm_plot');
Plotly.newPlot(BPMPlot, bpm_plot_data, bpm_plot_layout);

TriggerPressurePlot = document.getElementById('trigger_pressure_plot');
Plotly.newPlot(TriggerPressurePlot, trigger_pressure_plot_data, trigger_pressure_plot_layout);


function get_data() {
    let request = new XMLHttpRequest();
    request.open("GET", "/get_data", false);
    request.send(null);
    let new_data = JSON.parse(request.responseText);

    for (let i = 0; i < new_data.Pressure.x.length; i++) {
        pressure_plot_data[0].x.push(new_data.Pressure.x[i]);
        pressure_plot_data[0].y.push(new_data.Pressure.y[i]);
    }

    pressure_plot_data[0].x = pressure_plot_data[0].x.slice(-500);
    pressure_plot_data[0].y = pressure_plot_data[0].y.slice(-500);

    for (let i = 0; i < new_data.Volume.x.length; i++) {
        volume_plot_data[0].x.push(new_data.Volume.x[i]);
        volume_plot_data[0].y.push(new_data.Volume.y[i]);
    }

    volume_plot_data[0].x = volume_plot_data[0].x.slice(-500);
    volume_plot_data[0].y = volume_plot_data[0].y.slice(-500);

    for (let i = 0; i < new_data.bpm.x.length; i++) {
        bpm_plot_data[0].x.push(new_data.bpm.x[i]);
        bpm_plot_data[0].y.push(new_data.bpm.y[i]);
    }

    bpm_plot_data[0].x = bpm_plot_data[0].x.slice(-500);
    bpm_plot_data[0].y = bpm_plot_data[0].y.slice(-500);

    for (let i = 0; i < new_data.TriggerPressure.x.length; i++) {
        trigger_pressure_plot_data[0].x.push(new_data.TriggerPressure.x[i]);
        trigger_pressure_plot_data[0].y.push(new_data.TriggerPressure.y[i]);
    }

    trigger_pressure_plot_data[0].x = trigger_pressure_plot_data[0].x.slice(-500);
    trigger_pressure_plot_data[0].y = trigger_pressure_plot_data[0].y.slice(-500);

    Plotly.redraw(PressurePlot, pressure_plot_data, pressure_plot_layout);
    Plotly.redraw(VolumePlot, volume_plot_data, volume_plot_layout);
    Plotly.redraw(TriggerPressurePlot, trigger_pressure_plot_data, trigger_pressure_plot_layout);
    Plotly.redraw(BPMPlot, bpm_plot_data, bpm_plot_layout);
}

setInterval(get_data, 500);