
const SerialPort = require("serialport");

const Readline = SerialPort.parsers.Readline;

const arduino_port = new SerialPort("/dev/ttyACM0", {
    baudRate: 115200,
    parity: 'none',
    stopBits: 1,
    dataBits: 8,
    flowControl: false,
    usePromises: false
}, (err) => {
    if (err) {
        console.log('error: ', err.message);
        port.close();
    }
});

const arduino_parser = new Readline({ delimiter: '\r\n' });

arduino_port.pipe(arduino_parser);

function check_arduino(port, parser) {
    parser.on('data', (data) => {
        console.log(data + " " + new Date().toISOString());
    })
}

check_arduino(arduino_port, arduino_parser);